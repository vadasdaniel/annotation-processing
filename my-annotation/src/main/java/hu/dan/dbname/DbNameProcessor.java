package hu.dan.dbname;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Completion;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.*;
import javax.tools.Diagnostic;
import javax.tools.FileObject;
import javax.tools.StandardLocation;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Set;

@SupportedAnnotationTypes("hu.dan.dbname.DbName")
public class DbNameProcessor extends AbstractProcessor {
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {

        System.out.println("DbNameProcessor");

        for (Element annotatedElement : roundEnv.getElementsAnnotatedWith(DbName.class)) {
            if (annotatedElement instanceof VariableElement) {
                VariableElement field = (VariableElement) annotatedElement;
                String fieldName = field.getSimpleName().toString();

                String setterValue = field.getAnnotation(DbName.class).value();

                TypeElement classElement = (TypeElement) field.getEnclosingElement();
                String packageName = processingEnv.getElementUtils().getPackageOf(classElement).toString();
                String className = classElement.getSimpleName().toString();

                try {
                    FileObject resource = processingEnv.getFiler().createResource(StandardLocation.CLASS_OUTPUT, packageName, className);

                    Writer writer = resource.openWriter();

                    writer.append("    public void ").append(setterValue).append("(String value) {");
                    writer.append("        this.").append(fieldName).append(" = value;");
                    writer.append("    }");

                    writer.close();
                } catch (IOException ex) {
                    processingEnv.getMessager().printMessage(
                            Diagnostic.Kind.ERROR, "Failed to (create) generate setter method: " + ex.getMessage());
                }
//                try {
//
//                    FileObject resource = processingEnv.getFiler().getResource(StandardLocation.CLASS_OUTPUT, packageName, className);
//
//                    System.out.println(packageName);
//                    System.out.println(className);
//
//                    PrintWriter writer = (PrintWriter) resource.openWriter();
//
//                    writer.append("    public void ").append(setterValue).append("(String value) {");
//                    writer.append("        this.").append(fieldName).append(" = value;");
//                    writer.append("    }");
//
//                    writer.close();
//                } catch (IOException ex) {
//                    processingEnv.getMessager().printMessage(
//                            Diagnostic.Kind.ERROR, "Failed to (modify) generate setter method: " + ex.getMessage());
//                }
            }
        }

        return false;
    }
}
